##多租户模式mybatis-plus demo
###数据隔离方案
* 独立数据库
  > 一个租户一个数据库
* 共享数据库，独立 Schema
  > 多个或所有租户共享Database，但是每个租户一个Schema（也可叫做一个user）。底层库比如是：DB2、ORACLE等，一个数据库下可以有多个SCHEMA
* 共享数据库，共享 Schema，共享数据表 
  > 租户共享同一个Database、同一个Schema，但在表中增加TenantID多租户的数据字段
    
###mybatis-plus实现
* **共享数据库，共享 Schema，共享数据表**
* 租户标识(provider_id)
    
