package com.tom.context;

import com.alibaba.ttl.TransmittableThreadLocal;
import com.tom.domain.User;

public class UserContextHolder {

    /**
     * 支持父子线程之间的数据传递
     */
    private static final ThreadLocal<User> CONTEXT = new TransmittableThreadLocal<>();

    public static void setUser(User user) {
        CONTEXT.set(user);
    }

    public static User getUser() {
        return CONTEXT.get();
    }

    public static void clear() {
        CONTEXT.remove();
    }
}
