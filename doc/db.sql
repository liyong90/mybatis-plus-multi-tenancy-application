-- multitenancy.`user` definition

CREATE TABLE `user` (
                        `id` int unsigned NOT NULL AUTO_INCREMENT,
                        `provider_id` varchar(100) NOT NULL COMMENT '服务商id',
                        `name` varchar(100) NOT NULL COMMENT '姓名',
                        `is_del` char(1) NOT NULL DEFAULT '0' COMMENT '0:未删除 1删除',
                        PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COMMENT='用户表';