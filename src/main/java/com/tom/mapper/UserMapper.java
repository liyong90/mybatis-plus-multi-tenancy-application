package com.tom.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tom.domain.User;

public interface UserMapper extends BaseMapper<User> {

}
