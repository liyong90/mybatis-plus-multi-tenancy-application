package com.tom.context;

import com.alibaba.ttl.TransmittableThreadLocal;

public class TenantContextHolder {

    /**
     * 支持父子线程之间的数据传递
     */
    private static final ThreadLocal<Long> CONTEXT = new TransmittableThreadLocal<>();

    public static void setTenant(Long tenant) {
        CONTEXT.set(tenant);
    }

    public static Long getTenant() {
        return CONTEXT.get();
    }

    public static void clear() {
        CONTEXT.remove();
    }
}
